/*
 * WordCounterTest.cpp
 *
 *  Created on: Mar 8, 2014
 *      Author: aabou
 */

#include <fstream>
#include <iostream>

#include "WordCounter.hpp"

#include <gtest/gtest.h>

using namespace std;

TEST(WordCounterTest, Count) {
    ifstream aFile("/home/aabou/dev/cpp/zz-itii-ds1/data/email.txt");
    unsigned int count = 0;

    if (aFile) {
        string aLine;
        while(getline(aFile, aLine)) {
            WordCounter aWordCounter(aLine);
            count += aWordCounter.count();
        }
    } else {
        cerr << "Error: impossible to open the file." << endl;
    }
    EXPECT_EQ(333, count);
}
